

<!-- README.md is generated from README.qmd. please edit the latter. -->

## capmicromet: tools and workflow for quality checking micromet data and uploading to a database

### overview

The CAP LTER maintains two micromet stations in the greater Phoenix
Metropolitan area. Station data are downloaded as \*.dat files from
Campbell data loggers by project technicians approximately monthly. The
`capmicromet` package features a suite of tools to facilitate ingesting
the data from these files into the R environment to address
quality-control routines and upload the data to the database that houses
the long-term record of the observations. The tools featured in this
package are tailored to the format of the CAP LTER micromet station
data. As such, the tools in the form as they exist in the package would
be of limited value in other contexts. However, only minor tweaking
(especially variable names) would be required to expand the utility of
these tools to other applications. Further, the package serves as a base
for adding new quality-control or other data-manipulation tools.

### installation

Install from GitLab (after installing the
[devtools](https://cran.r-project.org/web/packages/devtools/index.html)
package:

``` r
devtools::install_gitlab("CAPLTER/capmicromet")
```

### workflow

Outlined below is a typical workflow for processing new micromet data.

#### load data

The workflow can accommodate a single \*.dat file or multiple files as
illustrated below.

``` r
data_files <- list.files(
  path        = "path_to_data",
  pattern     = "dbg",
  recursive   = FALSE,
  full.names  = TRUE,
  ignore.case = TRUE
)

tower_data <- purrr::map_df(
  .x = data_files,
  .f = capmicromet::harvest_tower_data
)
```

#### filter to include only new observations

Rather than forcing an overwite of existing data, which, although
readings from the same timestamps should be identical, we omit data with
timestamps that already exist in the database as a precautionay measure
in case readings with like timestamps but differing readings do exist
(see next section).

``` r
tower_data <- capmicromet::filter_new_observations(tower_data)
```

#### filter to include only unique observations

Duplicate values are common as data downloads frequently and typically
overlap such that the tail of a data download usually overlaps with the
head of the subsequent data download. In these cases, the readings are
almost always the same, in which the goal is to simply deduplicate those
records. However, there are occasions where duplicate records for a
site\*timestamp may have different readings. The reason for this is
unclear, but since the true value is unknown, all records should be
kept. To accomplish this, all records are assigned with a unique
replicate number, with unique replicate numbers assigned to those
records with identical site\*timestamp combinations but contrasting
readings (as determined by the standard deviation of the readings). The
`filter_unique_observations` function addresses both of these goals:
deduplicating identical data, and assigning a unique replicate number
for the rare records that share timestamps but different readings.

``` r
tower_data <- capmicromet::filter_unique_observations(tower_data)
```

Duplicate values should be considered carefully as duplicate
site\*timestamp combinations with unlike data values can indicate
problems with the data. Further, incorporating data with unique
replicates, albeit conservative, can contribute confusion. Consider, for
example, the following duplicates from an upload of numerous data files
from the LDP site. The duplicates span many files, resulting in an
excessive number of replicates (because each obseravation with the same
record ID is considered a replicate). Adding seven replicates of the
same observation would be inefficient and potentially confusing.
Further, the substantial variance in the readings from records 583283
and 643052 prompted a review of the data through which an error in the
data was identified.

| timestamp           | record | airtc_avg | rh    | slrkw_avg | slrmj_tot | ws_ms_avg | winddir | rain_mm_tot | site  | filename                 | replicate |
|---------------------|--------|-----------|-------|-----------|-----------|-----------|---------|-------------|-------|--------------------------|-----------|
| <dttm>              | <chr>  | <dbl>     | <dbl> | <dbl>     | <dbl>     | <dbl>     | <dbl>   | <dbl>       | <dbl> | <chr>                    | <int>     |
| 2019-12-16 02:10:00 | 583283 | 2.64      | 84.1  | 0         | 0         | 0.481     | 121\.   | 0           | 2     | LDP_02022021.dat         | 1         |
| 2019-12-16 02:10:00 | 583283 | 2.64      | 84.1  | 0         | 0         | 0.481     | 121\.   | 0           | 2     | ldp_03052020.dat         | 2         |
| 2019-12-16 02:10:00 | 583283 | 2.64      | 84.1  | 0         | 0         | 0.481     | 121\.   | 0           | 2     | LDP_04012021.dat         | 3         |
| 2019-12-16 02:10:00 | 583283 | 2.64      | 84.1  | 0         | 0         | 0.481     | 121\.   | 0           | 2     | LDP_04302020_renamed.dat | 4         |
| 2019-12-16 02:10:00 | 583283 | 2.64      | 84.1  | 0         | 0         | 0.481     | 121\.   | 0           | 2     | ldp_06112020.dat         | 5         |
| 2019-12-16 02:10:00 | 583283 | 2.64      | 84.1  | 0         | 0         | 0.481     | 121\.   | 0           | 2     | ldp_06112020.dat         | 6         |
| 2019-12-16 02:10:00 | 583283 | 2.64      | 84.1  | 0         | 0         | 0.481     | 121\.   | 0           | 2     | ldp_11252020.dat         | 7         |
| 2019-12-16 02:10:00 | 643052 | 9.25      | 93    | 0         | 0         | 1.07      | 264\.   | 0           | 2     | LDP_01282021_renamed.dat | 1         |
| 2019-12-16 02:10:00 | 643052 | 9.25      | 93    | 0         | 0         | 1.07      | 264\.   | 0           | 2     | LDP_04302020_renamed.dat | 2         |
| 2019-12-16 02:10:00 | 643052 | 9.25      | 93    | 0         | 0         | 1.07      | 264\.   | 0           | 2     | ldp_06112020.dat         | 3         |

#### check for gaps

With duplicates removed, we can use the `check_for_gaps` function to
examine the data for gaps in the time series.

``` r
capmicromet::check_for_gaps(tower_data)
```

#### check for extreme values

Based on historic data (i.e., all existing values in the database for a
given location), values of new data that exceed a specified quantile
(e.g., 99%) are flagged (value = 1) in a new column (x). check_range
will look for a column name `variable` that indicates the data have been
transformed from wide to long format, and will attempt to reformat the
data (to long) if that is not found.

For most variables, it makes sense only to test whether the reading
exceeds the upper range of values: extreme negative air temperature
values skew the lower bound of air temperature readings; relative
humidity, average and total solar radiation bottom at zero (0.05
quartile is zero); wind direction is programatically constrained to
within 0-360; and there is not enough variation in Rain_mm_Tot to
evaluate.

``` r
out_of_range <- capmicromet::check_range(tower_data) |>
  filter(x == 1)
```

#### upload data to the database

When satisfied, we can use the `upload_observations` function to upload
micromet event and observation data to the database. Uploading event and
observation data were separate steps (and functions) in an earlier
version of this package but uploading events has since been incorporated
into the `upload_observations` function (mostly to facilitate all
database interaction under a single transaction).

##### upload events and observations

``` r
capmicromet::upload_observations(tower_data)
```
