#' @title Check for gaps in observations
#'
#' @description check_for_gaps is a side-effect function that checks for gaps
#' in the timestamps of a data entity. The input should be the micromet data in
#' wide form where we would expect a single record for a given site*timestamp.
#'
#' @note check_for_gaps is only valid for data entities that feature one site,
#' a future version will accommodate data entities featuring multiple sites.
#'
#' @param data_entity
#'  (character) unquoted name of the target data entity in the R environment
#'
#' @importFrom tsibble as_tsibble has_gaps scan_gaps
#'
#' @examples
#' \dontrun{
#'
#' check_for_gaps(tower_data)
#'
#' }
#'
#' @export

check_for_gaps <- function(data_entity) {

  has_gaps <- tsibble::as_tsibble(
    x = data_entity,
    key = "replicate",
    index = "timestamp",
    regular = TRUE
    ) %>%
  tsibble::has_gaps()

if (any(has_gaps$.gaps)) {

  tsibble::as_tsibble(
    x = data_entity,
    key = "replicate",
    index = "timestamp",
    regular = TRUE
    ) %>%
  tsibble::scan_gaps()

} else {

  message("gap not detected")

}

}
